package com.bonial.mushopl.command

import com.bonial.mushopl.User
import grails.validation.Validateable

@Validateable
class UserCommand {
    String username
    String password
    String confirmPassword
    String email

    static constraints = {
        importFrom User

        confirmPassword validator: { val, obj ->
            obj.properties['password'] == val
        }
    }
}
