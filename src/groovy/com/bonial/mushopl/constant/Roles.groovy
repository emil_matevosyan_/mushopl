package com.bonial.mushopl.constant


enum Roles {
    USER('ROLE_USER')

    String name

    Roles(String name) {
        this.name = name
    }
}