import com.bonial.mushopl.Item
import com.bonial.mushopl.Role
import com.bonial.mushopl.User
import com.bonial.mushopl.UserRole
import com.bonial.mushopl.constant.Roles

class BootStrap {

    def init = { servletContext ->
        environments {
            development {
                bootstrapSpringSecurityDatabase()
            }
        }
    }

    def destroy = {
    }

    private void bootstrapSpringSecurityDatabase() {
        def userRole = null
        Role.withTransaction {
            userRole = Role.findByAuthority(Roles.USER.name)
            if (!userRole)
                userRole = new Role(authority: Roles.USER.name).save(failOnError: true)
        }

        User user = null
        User user1 = null
        User.withTransaction {
            user = User.findByEmail("user@example.com") ?: new User(
                    email: 'user@example.com',
                    username: 'user',
                    password: '1',
                    enabled: true,
            ).save(failOnError: true)



            user1 = User.findByEmail("user1@example.com") ?: new User(
                    email: 'user1@example.com',
                    username: 'user1',
                    password: '1',
                    enabled: true,
            ).save(failOnError: true)
        }

        def item1 = null
        def item2 = null
        def item3 = null
        def item4 = null
        Item.withTransaction {
            item1 = new Item(name: 'item1', user: user).save(failOnError: true)
            item2 = new Item(name: 'item2', user: user).save(failOnError: true)
            item3 = new Item(name: 'item3', user: user1).save(failOnError: true)
            item4 = new Item(name: 'item4', user: user1).save(failOnError: true)
        }

        UserRole.withTransaction {
            if (!user.authorities.contains(userRole)) {
                UserRole.create(user, userRole)
            }
        }
    }
}
