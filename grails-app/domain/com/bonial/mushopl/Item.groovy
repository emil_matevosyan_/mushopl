package com.bonial.mushopl

class Item {
    String name

    static belongsTo = [user: User]

    static constraints = {
        name blank: false
    }
}
