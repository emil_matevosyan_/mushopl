<g:applyLayout name="main">
    <div class="page page-tasks">
        <div class="row">
            <div class="col-md-2"></div>

            <div class="col-md-8">
                <section class="task-container" data-ng-controller="taskCtrl">

                    <p class="add-task">
                        <input type="text" id="newItem" placeholder="What needs to be done?"
                               class="form-control" autofocus>
                        <a class="submit-button" onclick="addItem()" style="cursor: pointer">
                            <span class="glyphicon glyphicon-plus"></span>
                        </a>
                    </p>

                    <hr/>

                    <div id="itemTablePanel">
                        <g:render template="items" model="[items: items]"/>
                    </div>
                </section>
            </div>

            <div class="col-md-2"></div>
        </div>
    </div>
</g:applyLayout>