<div class="row">
    <div class="col-md-12">
        <section class="panel panel-default">
            <div class="panel-heading"><strong><span class="glyphicon glyphicon-th"></span> Shopping List</strong></div>

            <div class="panel-body">
                <div class="table-responsive">
                    <g:if test="${items}">
                        <table id="allItemsTable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>Name</th>
                            </tr>
                            </thead>

                            <tbody>
                            <g:each in="${items}" var="item">
                                <tr>
                                    <td>${item?.name}</td>
                                </tr>
                            </g:each>
                            </tbody>
                        </table>
                    </g:if>
                </div>
            </div>
        </section>
    </div>
</div>