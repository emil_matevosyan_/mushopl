<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
<head>
    <title>MUSHOPL - Shopping List</title>
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic" rel="stylesheet"/>
    <link href="${resource(dir: 'css/font-awesome/css', file: 'font-awesome.min.css')}" rel="stylesheet"/>
    <link href="${resource(dir: '/css/bootstrap', file: 'bootstrap.css')}" rel="stylesheet"/>
    <link href="${resource(dir: '/css', file: 'main.css')}" rel="stylesheet"/>
    <link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}">
    <r:require module="application"/>
    <r:layoutResources/>
</head>

<body>

<header class="header navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="row">
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1"
                        class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span
                        class="icon-bar"></span></button>
                <section class="logo text-center">
                    <g:link controller="home" class="navbar-brand">MUSHOPL</g:link>
                </section>
            </div>

            <div id="navbar-collapse-1" class="collapse navbar-collapse">
                <sec:ifLoggedIn>
                    <div class="nav navbar-nav navbar-btn" style="margin-left: 350px"></div>
                </sec:ifLoggedIn>
                <ul class="nav navbar-nav navbar-right">
                    <li><g:link controller="home" action="index">Home</g:link></li>
                    <sec:ifLoggedIn>
                        <li><g:link controller="shopping" action="shoppingList"
                                    params="[username: sec.username()]">Shopping List</g:link></li>
                        <li>
                            <a href="javascript:void(0);" data-toggle="dropdown">
                                <span class="hidden-xs">
                                    <span>Welcome,  <sec:username/></span>
                                </span>
                            </a>
                        </li>
                        <li><g:link controller="logout"><span>Logout</span></g:link></li>
                    </sec:ifLoggedIn>
                    <sec:ifNotLoggedIn>
                        <li>
                            <g:link controller="login" action="auth">Sign In</g:link>
                        </li>
                    </sec:ifNotLoggedIn>
                </ul>
            </div>
        </div>
    </div>
</header>

<div class="content" style="margin-top: 100px">
    <g:layoutBody/>
    <g:render template="/common/hiddenLinks"/>
    <r:layoutResources/>
</div>


<footer class="footer text-center" style="color: beige">&copy; Copyright 2015 MUSHOPL</footer>
</body>
</html>

