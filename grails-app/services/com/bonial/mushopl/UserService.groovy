package com.bonial.mushopl

import com.bonial.mushopl.command.UserCommand
import com.bonial.mushopl.constant.Roles
import grails.transaction.Transactional
import groovy.util.logging.Slf4j

@Slf4j
@Transactional
class UserService {

    def createUser(UserCommand userCommand) {
        def user = new User()
        user.properties = userCommand.properties

        if (user.save(flush: true)) {
            log.info("Created user with id ${user.id}")
            addDefaultRole(user)
            return user
        } else {
            log.error("User creation attempt failed")
            log.error(user.errors.dump())
        }
    }

    private void addDefaultRole(User user) {
        def role = Role.findByAuthority(Roles.USER.name)
        UserRole.create(user, role)
    }
}
