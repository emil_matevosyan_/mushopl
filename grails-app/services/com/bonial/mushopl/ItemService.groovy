package com.bonial.mushopl

import grails.transaction.Transactional
import groovy.util.logging.Slf4j

@Slf4j
@Transactional
class ItemService {
    def springSecurityService

    def createItem(def params) {
        def item = new Item(name: params.name)
        item.user = springSecurityService.currentUser as User

        if (item.save(flush: true)) {
            log.info("Created item with id ${item.id}")
            return item
        } else {
            log.error("Item creation attempt failed")
            log.error(item.errors.dump())
        }
    }
}
