package com.bonial.mushopl

import grails.plugin.springsecurity.annotation.Secured
import groovy.util.logging.Slf4j
import org.springframework.transaction.annotation.Transactional

@Slf4j
@Secured(['IS_AUTHENTICATED_FULLY'])
class ShoppingController {
    static allowedMethods = [
            shoppingList: 'GET',
            addItem     : 'POST'
    ]
    def springSecurityService
    def itemService

    def shoppingList() {
        if (springSecurityService.currentUser?.username?.equals(params.username)) {
            def itemList = Item.findAllByUser(springSecurityService.currentUser as User)
            render(view: '/shopping/list', model: [items: itemList])
        } else {
            redirect(uri: '/not-found')
        }
    }

    @Transactional
    def addItem() {
        if (params.name) {
            def item = itemService.createItem(params)

            def itemList = Item.findAllByUser(springSecurityService.currentUser as User)
            if (item?.id) {
                render(template: 'items', model: [items: itemList])
            } else {
                log.error("Failed to create an item")
                render(view: 'list', model: [items: itemList])
            }
        }
    }
}
