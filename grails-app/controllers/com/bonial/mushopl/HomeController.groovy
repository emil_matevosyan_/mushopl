package com.bonial.mushopl

import grails.plugin.springsecurity.annotation.Secured

@Secured('permitAll')
class HomeController {
    static allowedMethods = [
            index: 'GET'
    ]

    def index() {
        render(view: '/index')
    }
}
