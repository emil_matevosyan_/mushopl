package com.bonial.mushopl

import com.bonial.mushopl.command.UserCommand
import grails.plugin.springsecurity.annotation.Secured
import groovy.util.logging.Slf4j
import org.springframework.transaction.annotation.Transactional

@Slf4j
@Secured('permitAll')
class AuthController {
    def springSecurityService
    def userService

    static allowedMethods = [
            signup  : 'GET',
            register: 'POST'
    ]

    def signup() {
        render(view: '/auth/signup')
    }

    @Transactional
    def register(UserCommand userCommand) {
        if (userCommand.hasErrors()) {
            render(view: 'signup', model: [userInstance: userCommand])
        } else {
            def user = userService.createUser(userCommand)

            if (user?.id) {
                redirect(controller: 'home')
            } else {
                log.error("Failed to register new user account")
                render(view: 'signup', model: [userInstance: userCommand])
            }
        }
    }
}
