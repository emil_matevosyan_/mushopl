package com.bonial.mushopl

import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(ShoppingController)
@Build([User, Item])
class ShoppingControllerSpec extends Specification {

    void "test shoppingList not found action"() {
        setup:
        params.username = 'emilan'
        controller.springSecurityService = [currentUser: null]

        when:
        controller.shoppingList()

        then:
        response.redirectedUrl == "/not-found"
    }

    void "test shoppingList action"() {
        setup:
        def user = User.build(username: 'emilan')
        params.username = 'emilan'
        controller.springSecurityService = [currentUser: user]
        def item = Item.build(user: user)

        when:
        controller.shoppingList()

        then:
        controller.modelAndView.viewName == "/shopping/list"
        controller.modelAndView.model.items == [item]
    }

    void "test addItem action when creation failed"() {
        setup:
        params.name = 'name'
        request.method = 'POST'
        def user = User.build(username: 'emilan')
        controller.springSecurityService = [currentUser: user]
        controller.itemService = [createItem: { params -> null }]

        when:
        controller.addItem()

        then:
        controller.modelAndView.viewName == '/shopping/list'
    }

    void "test addItem action"() {
        setup:
        params.name = 'name'
        request.method = 'POST'
        def user = User.build(username: 'emilan')
        controller.springSecurityService = [currentUser: user]
        views['/shopping/_items.gsp'] = 'mock template contents'
        def item = Item.build(id: 1, user: user)
        controller.itemService = [createItem: { params -> item }]

        when:
        controller.addItem()

        then:
        response.text == "mock template contents"
//        controller.modelAndView.model.items == [item]
    }
}
