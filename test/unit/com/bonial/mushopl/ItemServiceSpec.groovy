package com.bonial.mushopl

import grails.buildtestdata.mixin.Build
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

@Build(User)
@Mock(Item)
@TestFor(ItemService)
class ItemServiceSpec extends Specification {

    void "test item creation"() {
        setup:
        def user = User.build()
        service.springSecurityService = [currentUser: user]
        def params = [name: 'name']

        expect:
        service.createItem(params).name == 'name'
        service.createItem(params).user == user
    }
}
