package com.bonial.mushopl

import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import spock.lang.Unroll
import utils.ConstraintsUnitSpec

@Build(Item)
@TestFor(Item)
class ItemSpec extends ConstraintsUnitSpec {

    def setup() {
        mockForConstraintsTests(Item, [Item.build(name: 'name')])
    }

    @Unroll("Tested Item field #field with the value #val")
    def "test Item fields' constraints"() {
        when:
        def obj = new Item("$field": val)

        then:
        validateConstraints(obj, field, target)

        where:
        target     | field  | val
        'nullable' | 'name' | null
        'nullable' | 'name' | getEmptyString()
        'valid'    | 'name' | 'name'
    }
}
