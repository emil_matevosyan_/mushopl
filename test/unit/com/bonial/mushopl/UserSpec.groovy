package com.bonial.mushopl

import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import spock.lang.Unroll
import utils.ConstraintsUnitSpec

@Build(User)
@TestFor(User)
class UserSpec extends ConstraintsUnitSpec {
    def setup() {
        mockForConstraintsTests(User, [User.build(username: 'admin', password: '1', email: 'test@test.com')])
    }

    @Unroll("Tested User field #field with the value #val")
    void "test User fields' constraints"() {
        when:
        def obj = new User("$field": val)

        then:
        validateConstraints(obj, field, target)

        where:
        target     | field      | val
        'nullable' | 'username' | null
        'nullable' | 'username' | getEmptyString()
        'valid'    | 'username' | 'user'
        'nullable' | 'password' | getEmptyString()
        'valid'    | 'password' | '1'
        'email'    | 'email'    | getEmailText(false)
        'nullable' | 'email'    | null
        'nullable' | 'email'    | getEmptyString()
        'unique'   | 'email'    | 'test@test.com'
        'valid'    | 'email'    | getEmailText(true)
    }
}
