package com.bonial.mushopl

import com.bonial.mushopl.command.UserCommand
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */

@Mock([User, Role, UserRole])
@TestFor(UserService)
class UserServiceSpec extends Specification {

    void "test user creation"() {
        setup:
        def userCommand = new UserCommand(username: 'user', password: '1', confirmPassword: '1', email: 'test@test.com')

        expect:
        service.createUser(userCommand).username == 'user'
    }
}
