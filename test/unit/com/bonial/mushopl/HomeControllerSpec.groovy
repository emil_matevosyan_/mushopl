package com.bonial.mushopl

import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(HomeController)
class HomeControllerSpec extends Specification {
    void "test index action"() {
        when:
        controller.index()

        then:
        controller.modelAndView.viewName == '/index'
    }
}
