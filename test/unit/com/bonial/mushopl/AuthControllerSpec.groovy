package com.bonial.mushopl

import com.bonial.mushopl.command.UserCommand
import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import spock.lang.Specification

@Build(User)
@TestFor(AuthController)
class AuthControllerSpec extends Specification {

    void "test signup action"() {
        when:
        controller.signup()

        then:
        controller.modelAndView.viewName == '/auth/signup'
    }

    void "test register action invalid command"() {
        setup:
        request.method = 'POST'

        when:
        controller.register()

        then:
        controller.modelAndView.viewName == '/auth/signup'
    }

    void "test register action"() {
        setup:
        request.method = 'POST'
        def userCommand = new UserCommand(username: 'user', password: '1', confirmPassword: '1', email: 'test@test.com')
        def user = User.build(id: 2)
        controller.userService = [createUser: { command -> user }]

        when:
        controller.register(userCommand)

        then:
        response.redirectUrl == '/home'
    }

    void "test register invalid user"() {
        setup:
        request.method = 'POST'
        def userCommand = new UserCommand(username: 'user', password: '1', confirmPassword: '1', email: 'test@test.com')
        controller.userService = [createUser: { command -> null }]

        when:
        controller.register(userCommand)

        then:
        controller.modelAndView.viewName == '/auth/signup'
    }
}
