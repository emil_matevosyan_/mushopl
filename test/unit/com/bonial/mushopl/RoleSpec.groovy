package com.bonial.mushopl

import com.bonial.mushopl.constant.Roles
import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import spock.lang.Unroll
import utils.ConstraintsUnitSpec

@Build(Role)
@TestFor(Role)
class RoleSpec extends ConstraintsUnitSpec {

    def setup() {
        mockForConstraintsTests(Role, [Role.build(authority: Roles.USER.name)])
    }

    @Unroll("Tested Role field #field with the value #val")
    def "test Role fields' constraints"() {
        when:
        def obj = new Role("$field": val)

        then:
        validateConstraints(obj, field, target)

        where:
        target     | field       | val
        'nullable' | 'authority' | null
        'nullable' | 'authority' | getEmptyString()
        'unique'   | 'authority' | Roles.USER.name
    }
}