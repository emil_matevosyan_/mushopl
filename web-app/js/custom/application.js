$(document).ready(function () {
    addDataTable();
});

function addItem() {
    var name = $("#newItem").val();
    if (name != "") {
        $.ajax({
            url: $("#addItemURL").val(),
            type: 'POST',
            data: {
                name: name
            },
            success: function (data) {
                $("#itemTablePanel").find(':first-child').remove();
                $("#itemTablePanel").append(data);
                $("#newItem").val("");
                addDataTable();
            }
        });
    }

}

function addDataTable() {
    $("#allItemsTable").DataTable({
        "paging": true,
        "ordering": true,
        "info": false
    });
}